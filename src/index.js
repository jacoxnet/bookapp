const express = require('express');
const app = express();
require('dotenv').config();
const port = process.env.PORT || 4000;
const dbSetup = require('./database/setup');

// require routes
const bookRoutes = require('./routes/bookroutes');
const authRoutes = require('./routes/authRoutes');

// Seeders
const {seedAdmin} = require('./seeders/admin');

// setup express
app.use(express.json()); 

// setup db including mongoose
dbSetup();

// identify routes
app.use(bookRoutes);
app.use('/auth', authRoutes);

app.listen(port, () => console.log(`app listening on port ${port}`));