// set up mongoose
const mongoose = require('mongoose');
const connectionString = process.env.DB_URL;

// connect mongoose
module.exports = function () {
    mongoose.connect(connectionString, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true
    }, (err) => {
        if (err) console.log(`Error ${err}`);
        else console.log('database connection successful');
    });
};