const express = require('express');
const router = express.Router();
const bookctrl = require('../controllers/bookcontrollers');
const {authenticateUser, checkIfAdmin} = require('../middleware/authentication');

router.post('/books', authenticateUser, checkIfAdmin, bookctrl.createNewBook);

router.get('/books', authenticateUser, bookctrl.fetchAllBooks);

router.get('/books/:id', authenticateUser, bookctrl.fetchSingleBook);

router.put('/books/:id', authenticateUser, checkIfAdmin, bookctrl.updateSingleBook);

router.delete('/books/:id', authenticateUser, checkIfAdmin, bookctrl.deleteSingleBook);

module.exports = router;