// jwtoken stuff
const jwt = require('jsonwebtoken');
const SECRET = process.env.JWT_SECRET;
const EXPIRY = Number(process.env.TOKEN_EXPIRY);

exports.createToken = (user) => {
    try {
        let token = jwt.sign({
            id: user._id,
            username: user.username,
            firstName: user.firstName,
            lastName: user.lastName
        }, SECRET, {expiresIn: EXPIRY});
        return token;
        
    } catch (err) {
        console.log(err);
        return null;
    }   
};

exports.decodeToken = (token) => {
    console.log(`decodeToken ${token}`);
    let decodedToken = jwt.verify(token, SECRET);
    return decodedToken;
};
