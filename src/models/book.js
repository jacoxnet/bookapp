// create schema

const mongoose = require("mongoose");
const bookSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        minlength: 2
    },
    author: String,
    description: String,
    category: {
        type: String,
        enum: ["fiction", "non-fiction", "comics", "others"],
        default: "fiction"
    },
    purchaseCount: Number,
    imageUrl: String,
    tags: Array,
    color: String
});

module.exports = mongoose.model('Book', bookSchema);