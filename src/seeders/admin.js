const User = require('../models/user');
const bcrypt = require('bcrypt');
SALTROUNDS = 10;

// Default fields for new admin account 
const DEFAULTADMIN = {
    firstName: "Annie",
    lastName: "Administrator",
    username: "admin",
    role: "admin",
    password: ""
};
// Default password for admin account
ADMINPASS = 'mypass';

// add admin account if not one already
exports.seedAdmin = async () => {
    try {
        let admin = await User.findOne({role: 'admin'});
        if (admin) return 'admin account already exists';
        let hash = await bcrypt.hash(ADMINPASS, SALTROUNDS);
        DEFAULTADMIN.password = hash;
        await User.create(DEFAULTADMIN);
        return 'admin account created';
    } catch (err) {
        throw err;
    }
};
