const {decodeToken} = require('../services/jwtService');


exports.authenticateUser = async (req, res, next) => {
    var decodedToken;
    console.log('trying to authenticate');
    if (!req.headers.authorization) return res.status(401).json({message: 'authorization header required'});
    let aftersplit = req.headers.authorization.split(' ');
    if (aftersplit[0] !== 'Bearer') return res.json({message: "authorization format is 'Bearer <token>'"});
    try {
        let token = aftersplit[1];
        console.log('about to call decodeToken');
        decodedToken = decodeToken(token);
        console.log('after call decodeToken');
        
        if (!decodedToken) return res.status(401).json({message: 'user not found'});
    } catch (err) {
        return res.status(500).json({err});
    }
    console.log(`decodedToken: ${JSON.stringify(decodedToken)}`);
    req.user = decodedToken;
    next();
};

exports.checkIfAdmin = (req, res, next) => {
    if (req.user.role !== 'admin') return res.status(401).json({message: 'this route is restricted to admin users'});
    return next();
};