// Functions used in routes

const Book = require('../models/book');

// create a new book
exports.createNewBook = (req, res) => {
    // retrieve new book details from req.body
    const book = req.body;
    console.log({...req.body});
    Book.create({
        ...req.body
    }, (err, newBook) => {
        if (err) return res.status(500).json({
            message: err
        });
        else return res.status(200).json({
            message: 'new book created',
            newBook
        });
    });
};

// fetch all books
exports.fetchAllBooks = (req, res) => {
    console.log(req.user);
    let conditions = {};
    if (req.query.category) {
        conditions.category = req.query.category;
    }
    if (req.query.author) {
        conditions.author = req.query.author;
    }
    Book.find(conditions, (err, books) => {
        if (err) return res.status(500).json({
            message: err
        });
        return res.status(200).json({
            books
        });
    });
};

// fetch a single book by id
exports.fetchSingleBook = (req, res) => {
    Book.findById(req.params.id, (err, book) => {
        if (err) return res.status(500).json({
            message: err
        });
        else if (!book) return res.status(404).json({
            message: 'book not found'
        });
        return res.status(200).json({
            book
        });
    });
};

// update a single book by id
exports.updateSingleBook = (req, res) => {
    Book.findByIdAndUpdate(req.params.id, {
        title: req.body.title,
        category: req.body.category
    }, (err, book) => {
        if (err) return res.status(500).json({
            message: err
        });
        else if (!book) return res.status(404).json({
            message: 'book not found'
        });
        book.save((err, savedBook) => {
            if (err) return res.status(400).json({
                message: err
            });
            return res.status(200).json({
                message: 'book updated successfully'
            });
        });
    });
};

// delete a single book by id
exports.deleteSingleBook = (req, res) => {
    Book.findByIdAndDelete(req.params.id, (err, book) => {
        if (err) return res.status(500).json({
            message: err
        });
        else if (!book) return res.status(404).json({
            message: 'book not found'
        });
        return res.status(200).json({
            message: 'book deleted successfully'
        });
    });
};