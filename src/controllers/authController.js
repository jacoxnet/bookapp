// Functions used in authorization

// user model
const User = require('../models/user');
// bcrypt and constants
const bcrypt = require('bcrypt');
const SALTROUNDS = 10;
// jwtoken stuff
const {createToken} = require('../services/jwtService');

// register a new user
exports.registerNewUser = async (req, res) => {
    // make sure all fields completed
    if (!req.body.username || !req.body.password || !req.body.firstName || !req.body.lastName)
        return res.status(400).json({
            message: 'username, password, firstName, lastName required'
        });
    try {
        // see if username already in db
        let existingUser = await User.findOne({
            username: req.body.username
        });
        if (existingUser) return res.status(400).json({
            message: 'username already exists'
        });
        // hash the password
        let hash = await bcrypt.hash(req.body.password, SALTROUNDS);
        // create user in database with password field as the hashed password
        let requestedUser = {
            ...req.body
        };
        requestedUser.password = hash;
        let newUser = await User.create(requestedUser);
        // sign payload and return token
        const payload = {
            ...newUser,
        };
        // password should not be in payload
        delete payload.password;
        console.log('got to point');
        let token = createToken(payload);
        if (!token) return res.status(500).json({message: 'sorry, we could not authenticate you. please login'});
        return res.status(200).json({
            message: 'registration successful',
            token
        });
    } catch (err) {
        return res.status(500).json({
            message: 'caught err',
            err
        });
    }
};

// allow a user to login
exports.loginUser = async (req, res) => {
    // make sure all fields completed
    if (!req.body.username || !req.body.password)
        return res.status(400).json({
            message: 'username, password required'
        });
    try {
        // see if username already in db
        let retrievedUser = await User.findOne({
            username: req.body.username
        });
        if (!retrievedUser) return res.status(401).json({
            message: 'incorrect username'
        });
        let compareresult = await bcrypt.compare(req.body.password, retrievedUser.password);
        if (!compareresult) return res.status(401).json({
            message: 'Incorrect password'
        });
        const payload = {
            ...retrievedUser
        };
        // don't include password in payload
        delete payload.password;
        // sign payload for token 
        let token = createToken(payload);
        if (!token) return res.status(500).json({message: 'sorry, we could not authenticate you. please login'});
        return res.status(200).json({
            message: 'login successful',
            token
        });
    } catch (err) {
        return res.status(500).json({
            err
        });
    }
};