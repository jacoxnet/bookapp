# Book Store Application

## FEATURES

- Store owner can:
  - Create Books
  - Fetch Books
  - Update Books
  - Delete Books
  - Search for Books

## TO DO

- Set up mongoose
- Create Schema
- Create routes

## Book

- title
- author
- category
- purchaseCount
- imageUrl
- description
